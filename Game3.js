/**
 * Created by ASSET on 16-Aug-17.
 */
import React from 'react';
import {
    Text,
    View,
    LayoutAnimation,
    Alert,
    ScrollView,
    Image, StyleSheet, AsyncStorage
} from 'react-native';
import { StackNavigator } from 'react-navigation';

import {
    DragContainer,
    Draggable,
    DropZone
} from './dragndrop'


class MyDropZoneContent extends React.Component {

    constructor(props){
    super(props);
        this.name = props.name;
        this.dropped = props.dropped;
    }


    componentWillReceiveProps({dragOver}) {
        if (dragOver !== this.props.dragOver) LayoutAnimation.easeInEaseOut();
    }
    render() {
        return <View style={{width: this.props.dragOver ? 110 : 100, height:  this.props.dragOver ? 110 : 100, backgroundColor: '#00ffff', alignItems: 'center', justifyContent: 'center'}}>
            <View>
                <Text>{this.name}</Text>
            </View>
        </View>
    }
}


class DeleteZone extends React.Component {
    componentWillReceiveProps({dragOver}) {
        if (dragOver !== this.props.dragOver) LayoutAnimation.easeInEaseOut();
    }
    render() {
        return <View style={{top: this.props.dragOver ? 0: -100, height: 100, backgroundColor: 'red', alignItems: 'center', justifyContent: 'center'}}>
            <View>
                <Text>{'DELETE'}</Text>
            </View>
        </View>
    }
}

class DraggyInner extends React.Component {
    constructor(props){
        super(props);
        this.filename = props.filename;
        IMAGES = {
            bee: require('./img/bee.png'), // statically analyzed
            apple: require('./img/apple.png'), // statically analyzed
            bird: require('./img/bird.png'),
            butterfly: require('./img/butterfly.png'),
            cherries: require('./img/cherries.png'),
            fish: require('./img/fish.png'),
            pear: require('./img/pear.png'),
            grapes: require('./img/grapes.png'),

        };
    }


    getImage(string: str) { // dynamically invoked
        return IMAGES[str];
    }

    render() {
        if (this.props.dragOver && !this.props.ghost && !this.props.dragging) {
            return <View style={{height: 100, width: 100, backgroundColor: 'green'}} />
        }
        let shadows = {shadowColor: 'black', shadowOffset: {width: 0, height: 20}, shadowOpacity: .5, shadowRadius: 20, opacity: .5};
        return <View><Image source={IMAGES[this.filename]} style={styles.fruits}/></View>
    }
}


class Draggy extends React.Component {

    constructor(props){
        super(props);
        this.filename0 = props.filename0;
    }

    render() {
        return <Draggable data={this.filename0} style={{margin: 7.5}}>
            <DropZone>
                <DraggyInner  filename={this.filename0} />
            </DropZone>
        </Draggable>
    }
}

class Game3 extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Game3';
    }

    async saveEntry(key){
        try {
            let scount = await AsyncStorage.getItem(key);
            let icount = Number.parseInt(scount, 10) + 1;
            AsyncStorage.setItem(key, icount.toString());

        } catch (error) {
            // Error saving data
            Alert.alert("Exception in saveEntry: " + error);
        }
    }
/*
    saveEntry(key, value){
        AsyncStorage.setItem(key, value);
    }
    */

    static navigationOptions = {
        title: 'Drag & Drop',
    };


    render() {
        var dropzone1 = "grapes";
        var dropzone2 = "BEE";

        return <DragContainer>
            <DropZone style={{position: 'absolute', top: 0, left: 0, right: 0, height: 100}} onDrop={() => Alert.alert('DELETE!!!')}>
                <DeleteZone />
            </DropZone>
            <Image source={require('./img/bg4.jpg')} style={styles.backgroundImage}>
                <View>
                    <Text style={styles.welcome}>
                        Drag the image below and drop to an appropriate box with name.
                    </Text>
                </View>
                <View style={{flex: 1, padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
                    <DropZone onDrop={e => this.checkDnDItem(dropzone1, e.toString())}>
                        <MyDropZoneContent name={dropzone1} />
                    </DropZone>
                    <DropZone onDrop={e => this.checkDnDItem(dropzone2, e.toString())}>
                        <MyDropZoneContent name={dropzone2} />
                    </DropZone>
                </View>
                <View style={{height: 115}}>
                    <ScrollView horizontal={true}>
                        <View style={{justifyContent: 'center', alignItems: 'flex-end', flexDirection: 'row'}}>
                            <Draggy filename0="bee"/>
                            <Draggy filename0="apple"/>
                            <Draggy filename0="bird"/>
                            <Draggy filename0="butterfly"/>
                            <Draggy filename0="cherries"/>
                            <Draggy filename0="fish"/>
                            <Draggy filename0="grapes"/>
                            <Draggy filename0="pear"/>
                        </View>
                    </ScrollView>
                </View>
            </Image>
        </DragContainer>
    }

    /**
     *
     * @param item1
     * @param item2
     */
    checkDnDItem(item1, item2){
        if(item1.toLowerCase() == item2.toLowerCase()) {
            Alert.alert("Correct", "Good job. That's correct. It is a " + item2 + ".");
            this.saveEntry("G3OK");
        }
            else {
            Alert.alert("Wrong", "It is NOT a " + item1 + ". It is a " + item2 +  "." );
            this.saveEntry("G3NOT");
        }
    }
}


var styles = StyleSheet.create({
    welcome: {
        fontSize: 18,
        textAlign: 'center',
        margin: 5,
        color: 'white',
    },
    fruits:{
        width: 96,
        height: 96,
        margin: 20,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
});


export default Game3;

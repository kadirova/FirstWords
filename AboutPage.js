/**
 * Created by ASSET on 04-Sep-17.
 */

import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Image, Button, Alert, TouchableHighlight, TouchableOpacity, Animated,ScrollView} from 'react-native';
import { StackNavigator } from 'react-navigation';

class AboutPage extends React.Component {

    static navigationOptions = {
        title: 'About the app',
    };

    render() {
        return (
            <Image source={require('./img/bg4.jpg')} style={styles.backgroundImage}>
                <ScrollView>
                    <View style={styles.container_fruits}>
                        <Text style={styles.welcome}>
                            {"\n\n"}
                            Trinity College Dublin,
                            {"\n"}The University of Dublin
                            {"\n"}{"\n\n"}
                            School of Computer Science and Statistics. 2017 {"\n\n"}{"\n"}
                        </Text>
                        <Text style={styles.selectedItem}>
                            This application "First words" is developed for the course "CS7038 - GROUP PROJECT (INTERACTIVE ENTERTAINTMENT TECHNOLOGY)"{"\n\n"}
                            Lecturer: Dr. Carol  O'Sullivan{"\n\n"}
                            Student: Asset Kadirov (#16341293){"\n\n"}
                            Application title: First words{"\n"}{"\n\n"}

                            The application was developed with "React Native" framework
                        </Text>
                        <View>
                            <Image source={require('./img/rnlogo.png')} style={styles.rnlogo}></Image>
                        </View>
                    </View>
                </ScrollView>
            </Image>
        );
    }
}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 5,
        color: 'white',
        fontWeight: 'bold',
    },
    selectedItem: {
        fontSize: 16,
        textAlign: 'center',
        color: 'white',
    },
    container_fruits:{
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rnlogo:{
        width: 200,
        height: 103,
        margin: 20,
    },
    cont_fruits_text: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    center: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    circle: {
        borderRadius: 50,
        height: 100,
        width: 100,
        margin: 15
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
});



// This is important because without exporting PageOne,
// you cannot import it in SampleApp.js
export default AboutPage;

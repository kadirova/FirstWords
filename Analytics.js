/**
 * Created by ASSET on 04-Sep-17.
 */
import React from 'react';
import { StyleSheet, Text, View, AppRegistry, Image, Button, Alert, TouchableHighlight, TouchableOpacity, Animated,ScrollView, AsyncStorage} from 'react-native';
import { StackNavigator } from 'react-navigation';

class Analytics extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            G1OK: null,
            G1NOT: null,
            G2OK: null,
            G2NOT: null,
            G3OK: null,
            G3NOT: null,
            G4OK: null,
            G4NOT: null,
        }
    }

    async getAsynItem(item) {
        this.setState({
            G1OK: await AsyncStorage.getItem("G1OK"),
            G1NOT: await AsyncStorage.getItem("G1NOT"),
            G2OK: await AsyncStorage.getItem("G2OK"),
            G2NOT: await AsyncStorage.getItem("G2NOT"),
            G3OK: await AsyncStorage.getItem("G3OK"),
            G3NOT: await AsyncStorage.getItem("G3NOT"),
            G4OK: await AsyncStorage.getItem("G4OK"),
            G4NOT: await AsyncStorage.getItem("G4NOT"),
        });

    }

    clearStats(){
        AsyncStorage.setItem("G1OK", "0");
        AsyncStorage.setItem("G1NOT", "0");
        AsyncStorage.setItem("G2OK", "0");
        AsyncStorage.setItem("G2NOT", "0");
        AsyncStorage.setItem("G3OK", "0");
        AsyncStorage.setItem("G3NOT", "0");
        AsyncStorage.setItem("G4OK", "0");
        AsyncStorage.setItem("G4NOT", "0");

        this.setState({
            G1OK: "0",
            G1NOT: "0",
            G2OK: "0",
            G2NOT: "0",
            G3OK: "0",
            G3NOT: "0",
            G4OK: "0",
            G4NOT: "0",
    });

        Alert.alert("Cleared", "All statistics data has been wiped.");
    }
    componentDidMount() {
        this.getAsynItem();
    }







    static navigationOptions = {
        title: 'Analytics',
    };

    render() {
        return (
            <Image source={require('./img/bg3.jpg')} style={styles.backgroundImage}>
                <ScrollView>
                    <View>

                        <Text style={styles.welcome}>
                            Totals plays of all games:
                            {Number.parseInt(this.state.G1OK, 10) + Number.parseInt(this.state.G1NOT, 10) +
                                Number.parseInt(this.state.G2OK, 10) + Number.parseInt(this.state.G2NOT, 10) +
                                    Number.parseInt(this.state.G3OK, 10) + Number.parseInt(this.state.G3NOT, 10) +
                                        Number.parseInt(this.state.G4OK, 10) + Number.parseInt(this.state.G4NOT, 10)}
                            {'\n'}
                            C: {Number.parseInt(this.state.G1OK, 10) + Number.parseInt(this.state.G2OK, 10) + Number.parseInt(this.state.G3OK, 10)  + Number.parseInt(this.state.G4OK, 10)}
                            &nbsp;&nbsp;&nbsp;
                            W: {Number.parseInt(this.state.G1NOT, 10) + Number.parseInt(this.state.G2NOT, 10) +  Number.parseInt(this.state.G3NOT, 10) +  Number.parseInt(this.state.G4NOT, 10)}
                        </Text>
                        <Text>{'\n'}</Text>

                        <Text style={styles.textToLeftBold}>
                            Totals plays of Game "Match words and pictures": {Number.parseInt(this.state.G1OK, 10) + Number.parseInt(this.state.G1NOT, 10)}
                        </Text>
                        <Text style={styles.textToRight}>
                            Correct: {this.state.G1OK}
                        </Text>
                        <Text style={styles.textToRight}>
                            Wrong:  {this.state.G1NOT}
                        </Text>
                        <Text>{"\n"}</Text>


                        <Text style={styles.textToLeftBold}>
                            Totals plays of Game "Odd one out": {Number.parseInt(this.state.G2OK, 10) + Number.parseInt(this.state.G2NOT, 10)}
                        </Text>
                        <Text style={styles.textToRight}>
                            Correct: {this.state.G2OK}
                        </Text>
                        <Text style={styles.textToRight}>
                            Wrong:  {this.state.G2NOT}
                        </Text>
                        <Text>{"\n"}</Text>


                        <Text style={styles.textToLeftBold}>
                            Totals plays of Game "Drag & Drop": {Number.parseInt(this.state.G3OK, 10) + Number.parseInt(this.state.G3NOT, 10)}
                        </Text>
                        <Text style={styles.textToRight}>
                            Correct: {this.state.G3OK}
                        </Text>
                        <Text style={styles.textToRight}>
                            Wrong:  {this.state.G3NOT}
                        </Text>
                        <Text>{"\n"}</Text>


                        <Text style={styles.textToLeftBold}>
                            Totals plays of Game "Place the item": {Number.parseInt(this.state.G4OK, 10) + Number.parseInt(this.state.G4NOT, 10)}
                        </Text>
                        <Text style={styles.textToRight}>
                            Correct: {this.state.G4OK}
                        </Text>
                        <Text style={styles.textToRight}>
                            Wrong:  {this.state.G4NOT}
                        </Text>
                        <Text>{"\n\n"}</Text>
                    </View>
                    <View>
                        <View style={styles.container_fruits}>
                            <Button onPress={this.clearStats.bind(this)} title="Clear stats"/>
                        </View>
                    </View>
                </ScrollView>
            </Image>
        );
    }




}


var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 15,
        color: 'black',
        fontWeight: 'bold',
    },
    textToRight: {
        marginHorizontal: 10,
        fontSize: 14,
        textAlign: 'right',
        color: '#1194F6'
    },
    textToLeftBold: {
        marginHorizontal: 15,
        fontSize: 14,
        textAlign: 'left',
        color: '#1194F6',
        fontWeight: 'bold',
    },
    container_fruits:{
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    rnlogo:{
        width: 200,
        height: 103,
        margin: 20,
    },
    cont_fruits_text: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    center: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    circle: {
        borderRadius: 50,
        height: 100,
        width: 100,
        margin: 15
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null
    },
});



// This is important because without exporting PageOne,
// you cannot import it in SampleApp.js
export default Analytics;
